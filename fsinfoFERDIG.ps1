# directory som input
# hvor stor del av partisjonen som er full
# hvor mange filer finnes det ?
	# (Get-ChildItem .\iikos-oblig3_kraut_roe\).Count
# gjen filstorrelse, full path
# storrelse til den storste
#


$inp = $args[0]
		# sjekker at input er en mappe !
if ((get-item $inp) -is [system.io.directoryinfo]) {
	write-output $inp
	$antFiler = $(get-childitem $inp -recurse -file | measure-object | ForEach-Object{$_.Count})
		# finner path + storrelse til storste fil
	$storst = $((get-childitem $inp -r | sort -descending -property length | Select-Object -first 1 fullname, length).fullname)
	$ss = $(((get-childitem $inp -r | sort -descending -property length | Select-Object -first 1).length)/1KB)
		#Finner gjennomsnitt
	$tot = $(((Get-ChildItem testfolder -recurse -file | Measure-Object -property length -sum)).sum)/1KB
	$snitt = (($tot)/$antFiler)
	$mappe = $(get-item $args[0])
	$plass = $((((($mappe).PSDrive).Used) / (((($mappe).PSDrive).Free) + ((($mappe).PSDrive).Used))) * 100)
	write-output "Partisjonen $inp befinner seg paa er $plass% full"
	write-output "Det finnes $antFiler filer."
	write-output "Den storste er $storst - som er $ss KB"
	write-output "Gjennomsnittlig fil storelse er $snitt KB"
}
