# script som skriver total bruk av virtuel og fysisk minne
# a) totalt bruk av virtuelt minne (VirtualMemorySize)
# b) working set

#
#$path = "C:\Users\Admin\Documents\oblig3\iikos-oblig3_kraut_roe"


# TODO:
	# faa omgjort output til lesbart format
	# $PSScriptRoot\human // for aa finne root-adresse til annet script

######## ######## ######## ########

$L1 = "******** Minne info omprosess med PID $pid ********"

	# funker ikke siden programmet som skal kjores maa hardkodes lenger ned?...
#$aa=$PSScriptRoot
#$bb="\human-readable-bytes.ps1"
#$ep="$aa$bb"

	#ant arguments
$aa = $($args.Count)
	#dato
$date = $((Get-Date).ToLocalTime()).ToString("yyyy-MM-dd_hhmmss")

	#For alle arguments sendt
for ($i=0;$i-lt$aa;$i++){
		#opprett fil
	$name = "$($args[$i])-$date"
	#New-Item -path . -ItemType "file" -name $name
	#New-item -path . -name $name -itemtype "file"

	#write-output $name

		#skriv til fil linje1
	#Add-Content $name $L1

	$pa = ".\$name"
	write-output $pa
	#echo $L1 >> ".\$name"
	$("$L1" | out-file -filepath "$pa" -Append)

		######## a)
		#VirtualMemorySize /64
	$vm = $((Get-Process -id $($args[$i])).VirtualMemorySize)

	##########################################################
		#$vmO = ($(write-output $vm | $ep))
	$vmO = $(write-output $vm | C:\Users\Admin\Documents\oblig3\iikos-oblig3_kraut_roe\human-readable-bytes.ps1)

	$aop = "Totalt bruk av virtuelt minne: $vmO"
	#Add-Content $name "Totalt bruk av virtuelt minne: $vmO"
	#write-output "Totalt bruk av virtuelt minne: $vmO"
	$("$aop" | out-file -filepath "$pa" -Append)

		######## b)
		# storrelsen paa working set
	$ws = $((Get-Process -id $($args[$i])).WorkingSet)

	##########################################################
		#bruker et annet prog for aa gjore om til lesbar output
	$wsO = $(write-output $ws | C:\Users\Admin\Documents\oblig3\iikos-oblig3_kraut_roe\human-readable-bytes.ps1)

	$bop = "storreslse paa working set: $wsO"
	#Add-Content $name "storreslse paa working set: $wsO"
	#write-output "storrelse paa working set: $wsO"
	$("$bop" | out-file -filepath "$pa" -Append)
}
