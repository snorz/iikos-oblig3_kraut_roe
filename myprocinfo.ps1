
$meny="
    1 - Hvem er jeg og hva er nanvnet paa scriptet
    2-  Hvor lenge er det siden siste boot
    3 - Hvor mange prosesser og traader finnes?
    4 - Hvor mange context switch'er fant sted siste sekund
    5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund
    6 - Hvor mange interrupts fant sted siste sekund
    9 - Avslutt script
    "

write-output $meny

$valg = Read-Host '  '#input

switch ($valg)
{
        #hvem er jeg og hva er nanvnet paa scriptet
    1 { write-output "jeg er : $($env:UserName)"
	write-Output "path til fil $($PSCommandPath)"
	}

####### hvor lenge er det siden siste boot
    2 {
	$start=$((Get-CimInstance Win32_OperatingSystem).lastbootuptime)
	$end=$(Get-Date)
	$timespan= $end - $start
	$d = $timespan.days
	$t = $timespan.hours
	$m = $timespan.minutes
	write-output "Det er $d dager $t timer $m minutter siden boot."
	}

####### Hvor mange prosesser og traader finnes?
    3 {
	$antp=$((ps).count)
	$antt=$((((ps).Threads).id).count)
	write-output "ant prosesser : $antp ant traader : $antt"
	}

####### Hvor mange context switch'er fant sted siste sekund

    4 {		#Aner ikke om dette egt funker...
	#$antCS = $(((Get-Counter -Counter "\system\context switches/sec").CounterSamples).cookedvalue)

	#denne skal funke
	$antCS = $((Get-CimInstance win32_PerfFormattedData_perfOS_system).ContextSwitchesPersec)
	write-output " Det var $antCS sist sekund"
	}

####### Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund
    5 {
		#her er det 4 outputs, vet ikke hva som burde printes
		#	derfor kommer alle :)
	$userM = $((Get-CimInstance Win32_PerfFormattedData_Counters_ProcessorInformation).PercentUserTime)

	write-output "hvor mye tid brukt i User mode: $userM"
	}

####### Hvor mange interrupts fant sted siste sekund
    6 {
		# lese mer på interruptsPerSec
		#https://wutils.com/wmi/root/cimv2/win32_perfformatteddata_counters_processorinformation/#interruptspersec_properties
		#

	$antInt = $((Get-CimInstance Win32_PerfFormattedData_Counters_ProcessorInformation).InterruptsPersec)

		#Aner ikkehvorfor jeg får flere linjer. Sjekk ut senere.

	write-output "Ant interrupts per sec: $antInt"

	}

####### Avslutt script
    9 {write-output "hadeBra"
	}
}
