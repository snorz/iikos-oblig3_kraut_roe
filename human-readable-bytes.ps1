#
#convert bytes to readable number
#
#

#$1KB = 1024
#$1MB = 1048576
#$1GB = 1073741824

Set-Variable 1KB -option Constant -value 1024
Set-Variable 1MB -option Constant -value 1048576
Set-Variable 1GB -option Constant -value 1073741824

#$ab="$input"
#$a=[int64]$ab
$a=[int64]"$input"

#$a -is [int]

switch ($a) {
	{$_ -le 1023} {"$a B"  ;break;}
	{$_ -le 1048575} {"$($a/$1KB)KB";break;}
	{$_ -le 1073741823} {"$($a/1MB)MB" ;break;}
	{$_ -le 1099511627775} {"$($a/1GB)GB" ;break;}

}